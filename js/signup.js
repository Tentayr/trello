function checklenght(id,longmin,longmax)
{
  if($("#"+id).val().length > longmax || $("#"+id).val().length < longmin )
  {
        $("#"+id).css('background-color', 'red');
        $("#"+id+"icon").attr('class', 'glyphicon glyphicon-remove');
        $("#"+id+"icon").css('color', 'red');
        return false;
  }
  else
  {
        $("#"+id).css('background-color', 'green');
        $("#"+id+"icon").attr('class', 'glyphicon glyphicon-ok');
        $("#"+id+"icon").css('color', 'green');
        return true;
  }
}

function checksame(idsame,idfirst)
{
  if($("#"+idsame).val() !== $("#"+idfirst).val())
  {
        $("#"+idsame).css('background-color', 'red');
        $("#"+idsame+"icon").attr('class', 'glyphicon glyphicon-remove');
        $("#"+idsame+"icon").css('color', 'red');
        return false;
  }
  else
  {
        $("#"+idsame).css('background-color', 'green');
        $("#"+idsame+"icon").attr('class', 'glyphicon glyphicon-ok');
        $("#"+idsame+"icon").css('color', 'green');
        return true;
  }
}

function checkmail(id)
{
  if($("#"+id).val().indexOf("@") === -1 || $("#"+id).val().indexOf(".") === -1)
  {
    $("#"+id).css('background-color', 'red');
    $("#"+id+"icon").attr('class', 'glyphicon glyphicon-remove');
    $("#"+id+"icon").css('color', 'red');
    return false;
  }
  else
  {
    $("#"+id).css('background-color', 'green');
    $("#"+id+"icon").attr('class', 'glyphicon glyphicon-ok');
    $("#"+id+"icon").css('color', 'green');
    return true;
  }

}



$(document).ready(
    function()
    {
      $("#submit").click(function()
      {
          if(!checklenght("nom",2,15) || !checklenght("prenom",2,15) || !checklenght("password",5,20) || !checkmail("email") || !checksame("emailconf","email") || !checksame("passwordconf","password"))
          {
            return false;
          }
      });
    }
);
