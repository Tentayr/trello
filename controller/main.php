<?php
include('./view/menu.phtml');


if(isset($_SESSION['name']))
{
  if(isset($_GET["w"]))
  {
    $work = htmlspecialchars($_GET["w"]);
    switch ($work)
    {
      case 'logout':
        session_destroy();
        header("Location: ./");
        break;

      case 'Infos':
        include('./view/infos.phtml');
        break;

      case 'view':
        require 'view.php';
        break;

      case 'add':
        require 'add.php';
        break;

      case 'remove':
        require 'remove.php';
        break;

      case 'delete':
        require 'delete.php';
        break;

      default:
        include('./view/noob.phtml');
        break;
    }
  }
  else
  {
    include('./view/logged.phtml');
  }
}
else if(isset($_GET["w"]))
{
  $work = htmlspecialchars($_GET["w"]);
  switch ($work) {
    case 'inscription':
      include('./view/signup.phtml');
      break;

    case 'Human':
      include('./view/noob.phtml');
      break;

    case 'error':
      $probleme = "E-mail ou mot de passe incorrect.";
      include('./view/authenticate.phtml');
      break;


    default:
      include('./view/noob.phtml');
      break;
  }
}
else {
  include('./view/authenticate.phtml');
}





 ?>
