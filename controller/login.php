<?php
require('../model/managepublic.class.php');
require('../model/manageTable.class.php');
$mdp = htmlspecialchars($_POST['pwd']);
$mail = htmlspecialchars($_POST['email']);

$check = new Session;
$resultat = $check->checkVeracity($mail,$mdp);

if($resultat == false)
{
  header("Location: ../?w=error");
}
else
{
  $info = $check->returnInfo($resultat);
  session_start();
  $_SESSION['name'] = $info[0][3];
  $_SESSION['nom'] = $info[0][2];
  $_SESSION['mail'] = $info[0][1];
  $_SESSION['id'] = $info[0][0];
  $_SESSION['tableau'] = [];
  $table = new Table;
  $list = $table->getAllTable($_SESSION['id']);


  foreach ($list as $key)
  {
    array_push($_SESSION['tableau'], $key[0]);
  }


  header("Location: ../");
}

?>
