<?php
require('../model/managepublic.class.php');

$nom = htmlspecialchars($_POST['nom']);
$prenom = htmlspecialchars($_POST['prenom']);
$email = htmlspecialchars($_POST['mail']);
$password = htmlspecialchars($_POST['password']);

$testMail = new Session;
$resultat = $testMail->getmailDatabase($email);


if(isset($resultat[0][0]))
{
  header("Location: ../sorry.php");
}
else
{
  $register = new Session;
  $register->addUserDatabase($nom,$prenom,$email,$password);
  header("Location: ../welcome.php");
}

 ?>
