<?php
/* Created by Christophe. */
/* Edited : 07/02/2017 */
require('../config.php');
class Table
{
  private $db;

/*----------------------BOF Constructeur--------------------- */
  public function __construct()
  {
      try {
          $this->db = new PDO(DNS, USER, PSWD);//connexion à la BDD.
      } catch (PDOException $e) {//reception d'erreur
          if ($handle = fopen("./log/bd.log", "a+")){//on ouvre le fichier de log.
              fwrite($handle, $e->getMessage());//ecriture dans le fichier de log.
              fclose($handle);//fermeture du fichier de log.
          }
          die();//sortie du programme

      }
  }
/*----------------------EOF Constructeur--------------------- */

/*---------------------Renvoie de tout les tableaux d'un utilisateur------------------------*/
  public function getAllTable($id)
  {
      $req = $this->db->prepare("SELECT nom_table FROM tablelist WHERE tablelist.id_user = '$id'");
      $req->execute();
      $result = $req->fetchall(PDO::FETCH_NUM);
      return $result;
    }
/*---------------------Fin renvoie de tout les tableaux d'un utilisateur------------------------*/
/*---------------------Renvoie de tout les tableaux d'un utilisateur------------------------*/
  public function createNew($id,$name)
  {
    $date = date('Y-m-d');
    $req = $this->db->prepare("INSERT INTO tablelist(id_user, nom_table, date_creation) VALUES ('$id', '$name','$date')");
    $req->execute();
  }
/*---------------------Fin renvoie de tout les tableaux d'un utilisateur------------------------*/
/*---------------------Renvoie de tout les tableaux d'un utilisateur------------------------*/
  public function deleteTable($table)
  {
    $req= $this->db->prepare("DELETE FROM tablelist WHERE tablelist.nom_table = '$table'");
    $req->execute();
  }
/*---------------------Fin renvoie de tout les tableaux d'un utilisateur------------------------*/
/*---------------------Renvoie l'id d'un tableaux d'un utilisateur------------------------*/
  public function getTableId($name)
  {
      $req = $this->db->prepare("SELECT id_table FROM tablelist WHERE nom_table = '$name'");
      $req->execute();
      $result = $req->fetchall(PDO::FETCH_NUM);
      return $result[0][0];
    }
/*---------------------Fin renvoie id tableaux d'un utilisateur------------------------*/
}
?>
